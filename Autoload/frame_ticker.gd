extends Node

const FRAME_TICK_CEILING = 60
var frame_tick_available = -1
var frame_tick_current = -1

func get_frame_tick_queue():
	if frame_tick_available == FRAME_TICK_CEILING:
		frame_tick_available = 0
		return FRAME_TICK_CEILING
	else:
		frame_tick_available += 1
		return frame_tick_available

func _physics_process(delta):
	frame_tick_current += 1
	if frame_tick_current > FRAME_TICK_CEILING:
		frame_tick_current = 0
