extends Node

var start_time_unix = int(Time.get_unix_time_from_system())
#var cumulative_delta = 0

var message_display_time: int = 5 # seconds
var test: Array
var test_lookup: Array
var test_formatted: String

func _ready():
	test = []
	#print(start_time_unix)
	
func _process(delta):
	#cumulative_delta += delta
	test_formatted = ""
	for x in test.size():
		test_formatted += test[x]
		test_formatted += "\n"
	var current_time = int(Time.get_unix_time_from_system())
	if test_lookup.size() > 0:
		if test_lookup[0] < current_time:
			remove_message(0)

func add_message(message: String, display_time:= message_display_time):
	var size = test.size()
	var current_time = int(Time.get_unix_time_from_system())
	test.append(message)
	test_lookup.append(display_time + current_time)
	#print(display_time + current_time)

func remove_message(x):
	test.remove_at(x)
	test_lookup.remove_at(x)
