extends CharacterBody3D

class_name EnemyNPC

const SPEED = 5.0
const JUMP_VELOCITY = 2.2 #4.5

@onready var player : CharacterBody3D = get_tree().get_first_node_in_group("player")
@onready var anim : AnimatedSprite3D = $AnimatedSprite3D
@onready var nav_agent : NavigationAgent3D = $NavigationAgent3D
@onready var finger : RayCast3D = $RayCast3D
@onready var bloodgore = $Blood

@export var nodetest = false
@export var flags : Dictionary = {shart = 1, fart = 0}

#enum BSTATES {T1, T2, T3, T4}
#@export var nametest: BSTATES 
enum EnemyState {
	IDLE,
	PATROL,
	DETECTION,
	CHASE,
	ATTACK,
	POST_ATTACK,
	EVADE
}
@export var state := EnemyState.IDLE
var current_anim

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var friction = 2
#var sprite_rotation = 0
var anim_state = ""
var navving = false
var can_see_player = false
var player_in_bounds = false
var player_distance
var player_last_seen_pos
var just_detect_player = false
@export var shoot_timer_default = 2
var shoot_timer
var already_aiming = false

@export var damage = 5

var travel_direction

@export var health = 20
var dead : bool = false
var overkill = 0

var has_attacked = false

func _ready():
	#print(BSTATES.T1)
	#if flags.has("secret"):
		#print(flags["secret"])
	#var a=str(EnemyState.keys())
	#print(state_key())
	update_player_distance()
	shoot_timer = shoot_timer_default
	pass

func _process(delta):
	set_anim_state()
	if player_in_bounds:
		update_player_distance()
	#sprite_update()

func _physics_process(delta):
	velocity.y -= gravity * delta 
	if dead:
		velocity = lerp(velocity, Vector3.ZERO, friction * delta)
		move_and_slide()
		if is_on_floor() == true:
			#print(get_floor_normal())
			#velocity.y = velocity.y * -get_floor_normal().y
			#var newgrav = gravity * get_floor_normal().dot(ProjectSettings.get_setting("physics/3d/default_gravity_vector"))
			#velocity = velocity * get_floor_normal().dot(ProjectSettings.get_setting("physics/3d/default_gravity_vector"))
			pass
		return
	#update_travel(delta)
	#if navving:
		#update_travel()
		#velocity = velocity.lerp(travel_direction * SPEED, SPEED * delta)
		#look_at(nav_agent.target_position)
	## First one for changing state based on conditions
	match state:
		EnemyState.IDLE:
			if just_detect_player == true:
				state = EnemyState.DETECTION
		EnemyState.PATROL:
			#if within attack range
				#State = attack
			pass
		EnemyState.DETECTION:
			if player_distance < 5:
				state = EnemyState.ATTACK
			else:
				state = EnemyState.CHASE
		EnemyState.CHASE:
			if nav_agent.distance_to_target() < 3:
				just_detect_player = false
				state = EnemyState.DETECTION
			if player_distance < 10: ## TODO attack range
				state = EnemyState.ATTACK
				pass
		EnemyState.ATTACK:
			if has_attacked == true:
				has_attacked = false
				state = EnemyState.POST_ATTACK
		EnemyState.POST_ATTACK:
			pass
		EnemyState.EVADE:
			pass
	## Second for what to actually do for each state
	match state:
		EnemyState.IDLE:
			look_for_player()
			## run detection/sense function
				## if detect true, set state DETECTION
			#if abs(anim.ra2) < 33:
				#just_detect_player = true
		EnemyState.PATROL:
			pass
		EnemyState.DETECTION:
			detected_player()
		EnemyState.CHASE:
			chase()
			update_travel(delta)
		EnemyState.ATTACK:
			update_nav_target(global_position)
			update_travel(delta)
			attack(delta)
		EnemyState.POST_ATTACK:
			post_attack()
		EnemyState.EVADE:
			pass
	move_and_slide()

func state_key():
	return str(EnemyState.find_key(state))

func update_player_distance():
	player_distance = (position - player.position).length()
	#print(player_distance)

#region AI States
func update_nav_target(new_target: Vector3):
	nav_agent.target_position = new_target
	var new_target_distance = Vector2((global_position.x - new_target.x), global_position.z - new_target.z) 

func travel():
	navving = true

func update_travel(delta):
	travel_direction = nav_agent.get_next_path_position() - global_position
	if nav_agent.distance_to_target() < 1:
		navving = false
		nav_agent.target_position = position
		velocity = Vector3.ZERO
		look_at(player.position)
		#finger.force_raycast_update()
		#if finger.get_collider() == player:
			#update_nav_target(player.position)
		#else:
			#update_nav_target(player_last_seen_pos)
			#spotted_player = false
		return
	travel_direction = travel_direction.normalized()
	velocity = velocity.lerp(travel_direction * SPEED, SPEED * delta)
	look_at(nav_agent.target_position)
	

#func wander():
	#if state == "dead":
		#return
	#if navving == false:
		#update_nav_target(player.position - position)
	#if (player.position - position).length() > 5 and already_aiming == false:
		#aim()
	##velocity = (player.position - position).normalized() * SPEED
	#navving = true
	##velocity = nav_agent.velocity 
	##move_and_slide()
#

func look_for_player():
	if player_in_bounds == true:
		if abs(anim.ra2) < 33:
			just_detect_player = true
			return true
	pass

func detected_player():
	if just_detect_player:
		$DetectSound.play()
		just_detect_player = false

func chase():
	if navving == false:
		#var randompos = Vector3(position.x + randf_range(-10, 10), position.y, position.z + randf_range(-10, 10))
		#update_nav_target(randompos)
		
		##tried to make go in random -+45 degree angle towards player
		#var posdiff = global_position - player.global_position
		#posdiff = posdiff - global_position
		#posdiff.rotated(posdiff, deg_to_rad(randf_range(-45, 45)))
		#posdiff.normalized()
		#update_nav_target(posdiff)
		
		update_nav_target(player.position)
		navving = true
		return
	else:
		look_at(player.position)
		if finger.is_colliding():
			if finger.get_collider() == player:
				player_last_seen_pos = player.position
		if player_distance > nav_agent.distance_to_target():
			navving = false
	update_player_distance()
#
func aim():
	#state = EnemyState.ATTACK
	pass
#
func attack(delta):
	if player.dead == true:
		return
	navving = false
	shoot_timer -= delta
	if shoot_timer <= 0:
				$PainSound.play()
				shoot_timer = shoot_timer_default
				shoot()
				has_attacked = true
#
func shoot():
	#rotate_toward()
	look_at(player.position)
	if finger.is_colliding():
		var collider = finger.get_collider()
		if collider.has_method("hit"):
			collider.hit(damage, self)
#
func post_attack():
	var timer := Timer.new()
	add_child(timer) 
	timer.wait_time = 0.5 ## TODO add proper weapon cooldown var
	timer.one_shot = true
	timer.start()
	await timer	.timeout
	var rand = randi_range(1, 3)
	if rand == 1:
		state = EnemyState.ATTACK
	else:
		state = EnemyState.CHASE
#endregion

func set_anim_state():
	if dead:
		anim_state = "dead"
		return
	match state:
		EnemyState.IDLE:
			anim_state = state_key().to_lower()
			#print(anim_state)
		#EnemyState.ATTACK, "aim":
		EnemyState.ATTACK, EnemyState.CHASE:
			anim_state = "aim"
		"wander":
			anim_state = "idle"
		EnemyState.POST_ATTACK:
			anim_state = "shoot"

func hit(damage, hitter):
	if dead:
		return
	health -= damage
	if health <= 0:
		var healthdiff = health - damage
		kill(healthdiff)
		return
	look_at(hitter.position)
	$PainSound.play()

func kill(healthdiff):
	dead = true
	$DeathSound.play()
	anim_state = "dead"
	anim.change_anim(anim_state, true)
	$CollisionShape3D.disabled = true
	if bloodgore:
		bloodgore.emitting = 1
	#velocity += basis.z * healthdiff
	overkill = healthdiff
	var death_knockback = Vector3((player.global_position.x - global_position.x), (player.global_position.y - global_position.y), (player.global_position.z - global_position.z))
	death_knockback = death_knockback.normalized()
	velocity.x = 1 * healthdiff * death_knockback.x ## TODO maybe not use a magic number for healthdiff multiplication 
	#velocity.y *= 1 * healthdiff * death_knockback.z
	velocity.z = 1 * healthdiff * death_knockback.z
	move_and_slide()


func _on_area_3d_body_entered(body):
	if body == player:
		player_in_bounds = true

func _on_area_3d_body_exited(body):
	if body == player:
		player_in_bounds = false
