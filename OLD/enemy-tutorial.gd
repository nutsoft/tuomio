extends CharacterBody3D


@onready var animated_sprite_3d = $AnimatedSprite3D

@export var move_speed = 2.0
@export var attack_range = 2.0
@export var attack_cooldown = 1
@export var health = 15

@onready var player : CharacterBody3D = get_tree().get_first_node_in_group("player")
var dead = false
var attack_timer = 0
var can_attack = false

func _process(delta):
	attack_timer += delta
	if attack_timer > attack_cooldown:
		can_attack = true
	
func _physics_process(delta):
	if dead:
		return
	if player == null:
		return
	
	var dir = player.global_position - global_position
	dir.y = 0.0
	dir = dir.normalized()
	
	velocity = dir * move_speed
	move_and_slide()
	attempt_to_kill_player()

func attempt_to_kill_player():
	if can_attack == false:
		return
	var dist_to_player = global_position.distance_to(player.global_position)
	if dist_to_player > attack_range:
		return
	
	var eye_line = Vector3.UP * 1.5
	var query = PhysicsRayQueryParameters3D.create(global_position+eye_line, player.global_position+eye_line, 1)
	var result = get_world_3d().direct_space_state.intersect_ray(query)
	if result.is_empty():
		player.hit(20, self)
		attack_timer = 0
		can_attack = false

func hit(damage, hitter):
	health -= damage
	if health <= 0:
		kill()

func kill():
	dead = true
	$DeathSound.play()
	animated_sprite_3d.play("death")
	$CollisionShape3D.disabled = true
