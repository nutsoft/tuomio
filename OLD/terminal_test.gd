extends Node3D

@onready var term = $Terminal
@onready var termLabel = $Terminal/Panel/ColorRect/MarginContainer/ScrollContainer/Label
@onready var bg = $Terminal/Panel/ColorRect
@export_multiline var Text : String

var interactor

@export var trigger: SwitchTest
@onready var triggerbutton = $Terminal/Panel/TriggerButton
@export var triggerButtonText: String = "do eet"

func interact(player):
	#term.show()
	player.crosshair.hide()
	triggerbutton.text = triggerButtonText
	#player.look_at($Marker3D.global_position)
	
	#player.camera.look_at($Marker3D.global_position)
	
	#var tween = get_tree().create_tween()
	#tween.tween_property(player.camera, "rotation", $Marker3D.global_position, 5)
	#tween.tween_property($Sprite, "scale", Vector2(), 1)
	#tween.tween_callback($Sprite.queue_free)

	$HITS_Terminal_04/AnimationPlayer.play("Terminal_FlapAction")
	await $HITS_Terminal_04/AnimationPlayer.animation_finished
	var timer := Timer.new()
	add_child(timer) 
	timer.wait_time = 0.5
	timer.one_shot = true
	timer.start()
	#await timer.timeout
	term.show()
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	interactor = player

# Called when the node enters the scene tree for the first time.
func _ready():
	term.hide()
	termLabel.text = Text
	if trigger != null:
		triggerbutton.show()
	else:
		triggerbutton.hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#bg._set_size(termLabel.get_size())
	pass


func _on_button_pressed():
	term.hide()
	interactor.crosshair.show()
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	interactor.ui_mode = false
	$HITS_Terminal_04/AnimationPlayer.play_backwards("Terminal_FlapAction")




func _on_trigger_button_pressed():
	if trigger != null:
		if trigger.has_method("triggered"):
			trigger.triggered()
