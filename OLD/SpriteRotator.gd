extends AnimatedSprite3D

var ra2 = 0
@onready var player : CharacterBody3D = get_tree().get_first_node_in_group("player")
var sprite_rotation = 0.0
var state = ""
@onready var parent = $".."
@export var rand = 6

func change_anim(Nanimation = "", reset = false):
	var Nprogress = frame_progress
	var Nframe = frame
	play(Nanimation)
	#current_anim = Nanimation
	if reset:
		return
	else:
		set_frame_and_progress(Nframe, Nprogress)

func sprite_update(no_rotation: bool = false):
	var x = 0
	var flip = false
	var rotabs = abs(sprite_rotation)
	if rotabs >= 0 and rotabs < 22.5:
		x = 0
	if rotabs >= 22.5 and rotabs < 67.5:
		x = 45
		if sprite_rotation < 0:
			flip = true
	if rotabs >= 67.5 and rotabs < 112.5:
		x = 90
		if sprite_rotation < 0:
			flip = true
	if rotabs >= 112.5 and rotabs < 157.5:
		x = 135
		if sprite_rotation < 0:
			flip = true
	if rotabs >= 157.5:
		x = 180
	
	if no_rotation:
		change_anim(state)
	var y = state + "-" + str(x)
	#print(y)
	if flip:
		y = y + "f"
	change_anim(y)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	state = parent.anim_state
	#print(state)
	if parent.dead:
		return
	var rando = randi() % rand == 0
	if not rando:
		#print("zero")
		return
	ra2 = atan2((parent.global_position.x - player.global_position.x), (parent.global_position.z - player.global_position.z))
	ra2 = ra2 - parent.rotation.y
	#print($"..".rotation.y)
	ra2 = rad_to_deg(ra2)
	if ra2 < -180:
		ra2 = ra2 + 360
	if ra2 > 180:
		ra2 = ra2 - 360
	if parent.nodetest == true:
		#print(ra2)
		pass
	sprite_rotation = ra2
	if parent.dead:
		sprite_update(true)
		return
	sprite_update()
