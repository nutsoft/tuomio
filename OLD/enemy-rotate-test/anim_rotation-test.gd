extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 2.2 #4.5

@onready var player : CharacterBody3D = get_tree().get_first_node_in_group("player")
@onready var anim : AnimatedSprite3D = $AnimatedSprite3D

@export var nodetest = false
var current_anim

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var sprite_rotation = 0
var state = "idle"
var state_timer = 0.0
var ra2

func _ready():
	#anim.play("idle-0")
	sprite_update()
	
func change_anim(animation = "", reset = false):
	var progress = anim.frame_progress
	var frame = anim.frame
	anim.play(animation)
	current_anim = animation
	if reset:
		return
	else:
		anim.set_frame_and_progress(frame, progress)

func sprite_update():
	if state == "dead":
		return
	if state == "aim":
		change_anim(state)
	var x = 0
	var flip = false
	var rotabs = abs(sprite_rotation)
	if rotabs >= 0 and rotabs < 22.5:
		x = 0
	if rotabs >= 22.5 and rotabs < 67.5:
		x = 45
		if sprite_rotation < 0:
			flip = true
	if rotabs >= 67.5 and rotabs < 112.5:
		x = 90
		if sprite_rotation < 0:
			flip = true
	if rotabs >= 112.5 and rotabs < 157.5:
		x = 135
		if sprite_rotation < 0:
			flip = true
	if rotabs >= 157.5:
		x = 180

	var y = state + "-" + str(x)
	if flip:
		y = y + "f"
	#print(sprite_rotation)
	change_anim(y)

func _process(delta):
	if state == "aim":
		state_timer += delta
		if state_timer > 1:
			state = "shoot"
	match state:
		"dead":
			#kill()
			pass
	ra2 = atan2((global_position.x - player.global_position.x), (global_position.z - player.global_position.z))
	ra2 = ra2 - rotation.y
	ra2 = rad_to_deg(ra2)
	if ra2 < -180:
		ra2 = ra2 + 360
	if ra2 > 180:
		ra2 = ra2 - 360
	sprite_rotation = ra2
	sprite_update()

func _physics_process(delta):
	velocity.y -= gravity * delta 
	move_and_slide()

func kill():
	state = "dead"
	
	#print(anim.billboard)
	#anim.billboard = 3
	
	$DeathSound.play()
	#change_anim("death") #dont want to curry over current frame
	anim.play("death")
	$CollisionShape3D.disabled = true
