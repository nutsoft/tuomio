extends Label3D

@onready var finger = $"../RayCast3D"
@onready var player : CharacterBody3D = get_tree().get_first_node_in_group("player")
@onready var parent = $".."

var detect = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print(parent.anim.animation)
	#print(parent.anim.ra2)
	#text = str(parent.ra2)
	#parent.rotate_y(delta)
	#text = str(parent.velocity.y)
	
	text = str(parent.state_key())
	if parent.dead:
		text = "DEAD"
	if parent.flags:
		#text = str(parent.flags)
		#if parent.flags.has("overkill"):
			#text = str(parent.overkill)
		if parent.flags.has("label"):
			var test = parent.flags.get("label")
			if test != null:
				text = str(parent.get(str(test)))
