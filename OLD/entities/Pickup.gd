@tool
extends Area3D

@onready var sprite = $Sprite3D
@onready var sound = $AudioStreamPlayer
@export var texture : Texture
@export var audio: AudioStream 
@export var radius : float = 1
@export var ammo_type: int
@export var give : int = 5
@export var console_message: String

enum TYPES {AMMO, HEALTH, T3, T4}
@export var type_select: TYPES 

# Called when the node enters the scene tree for the first time.
func _ready():
	sprite.texture = texture
	$CollisionShape3D.shape.radius = radius


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Engine.is_editor_hint():
		if sprite.texture != texture:
			sprite.texture != texture
		return


func _on_body_entered(body):
	if body.is_in_group("player"):
		sound.play()
		var player : Player = body
		if type_select == 0:
			player.ammo_types[ammo_type] = player.ammo_types[ammo_type] + give
		if type_select == 1:
			player.health += give
		Console.add_message(console_message)
		await sound.finished
		queue_free()
