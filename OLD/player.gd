extends CharacterBody3D

class_name Player

#@onready var options_menu: Control = $%OptionsMenu

@onready var animated_sprite_2d = $CanvasLayer/GunBase/AnimatedSprite2D
@onready var camera = $Camera3D
@onready var ray_cast_3d = $Camera3D/RayCast3D
@onready var shoot_sound = $ShootSound
@onready var healthBar = $CanvasLayer/HealthBar
@onready var crosshair = $CanvasLayer/Crosshair2
@onready var pistol_ammoCounter = $CanvasLayer/AmmoUI/PistolAmmo
@onready var shotgun_ammoCounter = $CanvasLayer/AmmoUI/ShotgunAmmo
@onready var healthLabel = $CanvasLayer/HealthBar/HealthLabel

@export var _default_bullet_scene : PackedScene

@export var pain_sound: AudioStream
@export var death_sound: AudioStream

var metered = 0

const SPEED = 5.0
const MOUSE_SENS = 0.5

const JUMP_FORCE = 6 # 8
const ACCEL = 6
const ACCEL_AIR: float = 40
const TOP_SPEED_GROUND: float = 12
const TOP_SPEED_AIR: float = 2.5
const GRAVITY = 16

@export var health = 100.0

var grounded_prev: bool = true
var grounded: bool = true
var can_shoot = true
var dead = false
var wish_dir: Vector3 = Vector3.ZERO
var projected_speed: float = 0
# linearize friction below this speed value
var lin_friction_speed: float = 10 # 10
var friction: float = 4 #4
var terminal_velocity: float = GRAVITY * -5 # When this is reached, we stop increasing falling speed

var interaction_range = 5
var ui_mode = false
@export var freelook = true

@export var default_weapon: Weapon
@export var weapon1: Weapon
@export var weapon2: Weapon
@export var weapon3: Weapon
@export var weapon4: Weapon
var current_weapon: Weapon
var knockback = 0
var continue_shooting = false

var pistol_ammo = 10
var shotgun_ammo = 10

var ammo_types: Dictionary = {
	0: 0,
	1: pistol_ammo,
	2: shotgun_ammo
}

func GroundMove(delta):
	floor_snap_length = 0.4
	apply_acceleration(ACCEL, TOP_SPEED_GROUND, delta)
	
	if Input.is_action_pressed("jump"):
		velocity.y = JUMP_FORCE
	
	if grounded == grounded_prev:
		apply_friction(delta)
	
	#if is_on_wall:
		#clip_velocity(get_wall_normal(), 1, delta)

func AirMove(delta):
	apply_acceleration(ACCEL_AIR, TOP_SPEED_AIR, delta)
	
	clip_velocity(get_wall_normal(), 10, delta)
	clip_velocity(get_floor_normal(), 10, delta)
	
	velocity.y -= GRAVITY * delta 
	if velocity.y < 0:
		if velocity.y < terminal_velocity:
			velocity.y = terminal_velocity

func apply_acceleration(acceleration: float, top_speed: float, delta):
	var speed_remaining: float = 0
	var accel_final: float = 0
	
	speed_remaining = (top_speed * wish_dir.length()) - projected_speed
	
	if speed_remaining <= 0:
		return
	
	accel_final = acceleration * delta * top_speed
	
	clampf(accel_final, 0, speed_remaining)
	
	velocity.x += accel_final * wish_dir.x
	velocity.z += accel_final * wish_dir.z

func apply_friction(delta):
	var speed_scalar: float = 0
	var friction_curve: float = 0
	var speed_loss: float = 0
	var current_speed: float = 0
	
	# using projected velocity will lead to no friction being applied in certain scenarios
	# like if wish_dir is perpendicular
	# if wish_dir is obtuse from movement it would create negative friction and fling players
	current_speed = velocity.length()
	
	if(current_speed < 0.1):
		velocity.x = 0
		velocity.y = 0
		return
	
	friction_curve = clampf(current_speed, lin_friction_speed, INF)
	speed_loss = friction_curve * friction * delta
	speed_scalar = clampf(current_speed - speed_loss, 0, INF)
	speed_scalar /= clampf(current_speed, 1, INF)
	
	velocity *= speed_scalar

func clip_velocity(normal: Vector3, overbounce: float, delta) -> void:
	var correction_amount: float = 0
	var correction_dir: Vector3 = Vector3.ZERO
	var move_vector: Vector3 = get_velocity().normalized()
	
	correction_amount = move_vector.dot(normal) * overbounce
	
	correction_dir = normal * correction_amount
	velocity -= correction_dir
	
	## this is only here cause I have the gravity too high by default
	## with a gravity so high, I use this to account for it and allow surfing
	#velocity.y -= correction_dir.y * (GRAVITY/20)

func change_weapon_anim(current_weapon):
	animated_sprite_2d.position = current_weapon.position
	animated_sprite_2d.rotation = current_weapon.rotation
	animated_sprite_2d.scale = current_weapon.scale
	animated_sprite_2d.skew = current_weapon.skew
	shoot_sound.stream = current_weapon.sound_shoot
	animated_sprite_2d.play(current_weapon.sprite_idle)
	knockback = current_weapon.knockback
	if current_weapon.crosshair:
		crosshair.show()
		crosshair.texture = current_weapon.crosshair
	else:
		crosshair.hide()

func updateUI():
	pistol_ammoCounter.text = "Pistol: " + str(ammo_types[1])
	shotgun_ammoCounter.text = "Shotgun: " + str(ammo_types[2])

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	animated_sprite_2d.animation_finished.connect(shoot_anim_done)
	current_weapon = default_weapon
	change_weapon_anim(current_weapon)
	animated_sprite_2d.play(current_weapon.sprite_idle)
	$CanvasLayer/DeathScreen/Panel/Button.button_up.connect(restart)

func _input(event):
	#if event.as_text() == "Tab":
		#MenuTemplateManager.open_pause_menu()
	if event.as_text() == "Equal":
		scale *= Vector3(1.1,1.1,1.1)
	if event.as_text() == "Minus":
		scale *= Vector3(.9,.9,.9)
	if dead:
		return
	if ui_mode:
		return
	if event is InputEventMouseMotion:
		rotation_degrees.y -= event.relative.x * MOUSE_SENS
		if freelook:
			$Camera3D.rotation_degrees.x -= event.relative.y * MOUSE_SENS
			$Camera3D.rotation_degrees.x = clampf($Camera3D.rotation_degrees.x, -90, 90)
	#if event is InputEventJoypadMotion:
		#rotation_degrees.y -= event.axis_value * MOUSE_SENS

func _process(_delta):
	$FPS_meter.text = str(Engine.get_frames_per_second())
	$CanvasLayer/MessageBox/RichTextLabel.text = str(Console.test_formatted)
	#var meter = Vector2(velocity.x, velocity.z)
	metered = str(Vector2(velocity.x, velocity.z).length())
	updateUI()
	if metered:
		$Meter.text = str(metered)
	healthBar.set_value_no_signal(health)
	healthLabel.text = str(health)
	if Input.is_action_just_pressed("exit"):
		get_tree().quit()
	if Input.is_action_just_pressed("restart"):
		restart()
	if Input.is_action_just_pressed("weaponSlot1"):
		current_weapon = weapon1
	if Input.is_action_pressed("weaponSlot2"):
		current_weapon = weapon2
		change_weapon_anim(current_weapon)
	if Input.is_action_pressed("weaponSlot3"):
		current_weapon = weapon3
		change_weapon_anim(current_weapon)
	if Input.is_action_pressed("weaponSlot4"):
		current_weapon = weapon4
		change_weapon_anim(current_weapon)
	
	if Input.is_action_just_released("shoot"):
		continue_shooting = false
	
	if dead:
		return
	if ui_mode:
		return
	if Input.is_action_just_pressed("shoot") or continue_shooting:
		shoot()
		continue_shooting = true
	if Input.is_action_just_pressed("interact"):
		interact()

func _physics_process(delta):
	if dead:
		return
	if ui_mode:
		return
	grounded_prev = grounded
	var input_dir = Input.get_vector("move_left", "move_right", "move_forwards", "move_backwards")
	rotation_degrees.y += Input.get_action_strength("look_left") * MOUSE_SENS * 2
	rotation_degrees.y -= Input.get_action_strength("look_right") * MOUSE_SENS * 2
	wish_dir = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	#print(velocity.length() / TOP_SPEED_GROUND)
	projected_speed = (velocity * Vector3(1, 0, 1)).dot(wish_dir)
	if not is_on_floor():
		wish_dir *= 0.5 / 2
		grounded = false
		AirMove(delta)
	if is_on_floor():
		if velocity.y > 10:
			grounded = false
			AirMove(delta)
		else:
			grounded = true
			GroundMove(delta)
	#queue_jump()
	move_and_slide()


func restart():
	get_tree().reload_current_scene()

func shoot():
	if not can_shoot:
		return
	if current_weapon.ammo_use > ammo_types[current_weapon.ammo_type]:
		return
	ammo_types[current_weapon.ammo_type] = ammo_types[current_weapon.ammo_type] - current_weapon.ammo_use
	can_shoot = false
	animated_sprite_2d.play(current_weapon.sprite_shoot)
	shoot_sound.play()
	velocity += basis.z * knockback ## TODO maybe, add knockback to wishdir instead?
	#if ray_cast_3d.is_colliding() and ray_cast_3d.get_collider().has_method("hit"):
		#ray_cast_3d.get_collider().hit(current_weapon.damage)
	for n in current_weapon.shot_count:
		#print(n)
		var shot_step = 1.0 / current_weapon.shot_count
		#print(shot_step)
		var spread_mod = (shot_step * n)
		if continue_shooting:
			spread_mod = 1
			pass
		#print(shot_step)
		var bulletInst = _default_bullet_scene.instantiate() as Node3D
		bulletInst.set_as_top_level(true)
		get_parent().add_child(bulletInst)
		#ray_cast_3d.target_position.x = randf_range(-current_weapon.h_spread, current_weapon.h_spread)
		#ray_cast_3d.target_position.y = randf_range(-current_weapon.v_spread, current_weapon.v_spread)
		var x_spread = randf_range(-current_weapon.v_spread * (spread_mod), current_weapon.v_spread * (spread_mod))
		var y_spread = randf_range(-current_weapon.h_spread * (spread_mod), current_weapon.h_spread * (spread_mod))
		ray_cast_3d.rotation_degrees.y = y_spread
		ray_cast_3d.rotation_degrees.x = x_spread
		bulletInst.global_transform.origin = ray_cast_3d.get_collision_point() as Vector3 
		bulletInst.look_at((ray_cast_3d.get_collision_point()+ray_cast_3d.get_collision_normal()),Vector3.UP) ## TODO - fix disparity between bullet impact decal and impact sprite for pistol
		#if n == 0:
			#ray_cast_3d.rotation_degrees.x = 0
			#ray_cast_3d.rotation_degrees.y = 0
		ray_cast_3d.force_raycast_update()
		if !ray_cast_3d.is_colliding(): continue # Don't create impact when raycast didn't hit
		var collider = ray_cast_3d.get_collider()
		# Hitting an enemy
		if collider.has_method("hit"):
			collider.hit(current_weapon.damage, self)
		# Creating an impact animation	You have no idea what communism even is. Nothing about the modern Democratic Party has anything to do with communism. You sound like a moron

		var impact = preload("res://OLD/entities/fd_puff.tscn")
		var impact_instance = impact.instantiate()		
		impact_instance.play("shot")		
		get_tree().root.add_child(impact_instance)		
		impact_instance.position = ray_cast_3d.get_collision_point() + (ray_cast_3d.get_collision_normal() / 10)
		impact_instance.look_at($Camera3D.global_transform.origin, Vector3.UP, true)
		ray_cast_3d.rotation_degrees.x = 0
		ray_cast_3d.rotation_degrees.y = 0

func shoot_anim_done():
	can_shoot = true

func kill():
	dead = true
	$CanvasLayer/DeathScreen.show()
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	$ShootSound.set_stream(death_sound)
	$ShootSound.play()

func interact():
	if ray_cast_3d.is_colliding() and ray_cast_3d.get_collider().has_method("interact"):
			if Vector3(ray_cast_3d.get_collider().global_position - global_position).length() <= interaction_range:
				ui_mode = true
				ray_cast_3d.get_collider().interact(self)

func hit(damage, hitter):
	health -= damage
	if health <= 0:
		kill()
