extends Area3D

#@export var default_vector = -basis.z
@export var target_position: Vector3 = Vector3(0,0,0)
@onready var player : CharacterBody3D = get_tree().get_first_node_in_group("player")
var target_vector
var caster

func update_target():
	target_vector = global_position.direction_to(target_position)
	#if $target:
		#target_vector = global_position.direction_to($target.global_position)

# Called when the node enters the scene tree for the first time.
func _ready():
	update_target()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#rotate_toward()
	#velocity = default_vector * delta
	#move_and_collide(default_vector)
	#position = global_position + (target_vector / 10)
	#update_target()
	#if Vector3(target_position - global_position).length() < 0.1:
		#queue_free()
	#if $target:
		#$target.position += Vector3.FORWARD * delta
	pass

func _physics_process(delta):
	position = global_position + (target_vector / 5)
	


func _on_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if body == caster:
		return
	if body.has_method("hit"):
		body.hit(20, caster)
	queue_free()

func hit(hitter):
	queue_free()
