extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 2.2 #4.5

@onready var player : CharacterBody3D = get_tree().get_first_node_in_group("player")
@onready var anim : AnimatedSprite3D = $AnimatedSprite3D
@onready var nav_agent : NavigationAgent3D = $NavigationAgent3D
@onready var finger : RayCast3D = $RayCast3D
@onready var bloodgore = $Blood

@export var nodetest = false
@export var flags : Dictionary = {shart = 1, fart = 0}
#enum BSTATES {T1, T2, T3, T4}
#@export var nametest: BSTATES 
enum EnemyState {
	IDLE,
	PATROL,
	DETECTION,
	CHASE,
	ATTACK
}
var current_anim

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
#var sprite_rotation = 0
var state = ""
var anim_state = ""
var state_timer = 0.0
var ra2
var navving = false
var detect_player = false
var can_see_player = false
var player_in_bounds = false
var player_distance
var player_last_seen_pos
var spotted_player = false

@onready var shoot_timer = $Timer
var already_aiming = false

var travel_direction

@export var health = 20

func _ready():
	#print(BSTATES.T1)
	#if flags.has("secret"):
		#print(flags["secret"])
	state = "idle"
	pass

func update_player_distance():
	player_distance = (position - player.position).length()
	#print(player_distance)

#region AI States
func update_nav_target(new_target: Vector3):
	nav_agent.target_position = new_target
	var new_target_distance = Vector2((global_position.x - new_target.x), global_position.z - new_target.z) 
	#if new_target_distance.length() < 10 and (already_aiming == false):
		#print("attack!")
		#navving = false
		#already_aiming = true
		#state = "aim"

func travel():
	navving = true

func update_travel():
	travel_direction = nav_agent.get_next_path_position() - global_position
	if nav_agent.distance_to_target() < 1:
		navving = false
		state = "idle"
		nav_agent.target_position = position
		velocity = Vector3.ZERO
		look_at(player.position)
		finger.force_raycast_update()
		if finger.get_collider() == player:
			update_nav_target(player.position)
		else:
			update_nav_target(player_last_seen_pos)
			spotted_player = false
		#return
	travel_direction = travel_direction.normalized()

#func wander():
	#if state == "dead":
		#return
	#if navving == false:
		#update_nav_target(player.position - position)
	#if (player.position - position).length() > 5 and already_aiming == false:
		#aim()
	##velocity = (player.position - position).normalized() * SPEED
	#navving = true
	##velocity = nav_agent.velocity 
	##move_and_slide()
#
func detected_player():
	if spotted_player == false:
		$DetectSound.play()
	spotted_player = true
	if player_distance < 5: 
		shoot()
	if player_distance < 10:
		aim()
	update_nav_target(player.position)
	travel()
	#wander()
#
func aim():
	state = "aim"
	#navving = false
	#already_aiming = true
	#state = "aim"
	#shoot_timer.start()
#
func shoot():
	state = "shoot"

#endregion

func set_anim_state():
	match state:
		"dead", "idle", "shoot", "aim":
			anim_state = state
		"wander":
			anim_state = "idle"

func _process(delta):
	set_anim_state()
	#if nodetest:
		#if state == "shoot":
			#state = "wander"
	#if state == "aim":
		#state_timer += delta
		#if state_timer > 1:
			#state = "shoot"
			#state_timer = 0
	match state:
		"dead":
			#kill()
			return
		"wander":
			#wander()
			pass
		"aim":
			#aim()
			pass
		"shoot":
			navving = false
			#shoot()
	if player_in_bounds:
		update_player_distance()
		if abs(anim.ra2) < 33:
			detect_player = true
			player_last_seen_pos = player.position
	if detect_player == true and state != "hunt":
		state = "hunt"
		detected_player()
	#sprite_update()

func _physics_process(delta):
	velocity.y -= gravity * delta 
	var friction = 2
	if state == "dead":
		velocity = lerp(velocity, Vector3.ZERO, friction * delta)
		move_and_slide()
		return
	if navving:
		update_travel()
		velocity = velocity.lerp(travel_direction * SPEED, SPEED * delta)
		look_at(nav_agent.target_position)
	move_and_slide()

func hit(damage):
	if state == "dead":
		return
	health -= damage
	if health <= 0:
		var healthdiff = health - damage
		kill(healthdiff)

func kill(healthdiff):
	state = "dead"
	$DeathSound.play()
	anim.change_anim(state, true)
	$CollisionShape3D.disabled = true
	if bloodgore:
		bloodgore.emitting = 1
	#velocity += basis.z * healthdiff
	var death_knockback = Vector2((player.global_position.x - global_position.x), (player.global_position.z - global_position.z))
	death_knockback = death_knockback.normalized()
	velocity.x = 2 * healthdiff * death_knockback.x ## TODO maybe not use a magic number for healthdiff multiplication 
	velocity.z = 2 * healthdiff * death_knockback.y
	move_and_slide()


func _on_area_3d_body_entered(body):
	if body == player:
		player_in_bounds = true
		if abs(anim.ra2) < 33:
			detect_player = true

func _on_area_3d_body_exited(body):
	if body == player:
		player_in_bounds = false


func _on_timer_timeout():
	#print("Shoot!")
	already_aiming = false
	state = "shoot"
