extends Resource
class_name Weapon

enum AmmoTypes {MELEE, PISTOL, SHOTGUN, PLASMA}

@export_subgroup("Sprite and Transform")
#@export var model: PackedScene  # Model of the weapon
#@export var position: Vector3  # On-screen position
#@export var rotation: Vector3  # On-screen rotation
#@export var muzzle_position: Vector3  # On-screen position of muzzle flash
@export var sprite_idle: String
@export var sprite_shoot: String
@export var position: Vector2
@export var rotation: float
@export var scale: Vector2
@export var skew: float

@export_subgroup("Properties")
@export var ammo_type: AmmoTypes
@export_range(1, 500) var ammo_use: int = 1
#@export_range(0.1, 1) var cooldown: float = 0.1  # Firerate
#@export_range(1, 20) var max_distance: int = 10  # Fire distance
@export_range(0, 100) var damage: float = 1  # Damage per hit
@export_range(0, 1080) var h_spread: float = 0  # Spread of each shot sidewise
@export_range(0, 1080) var v_spread: float = 0  # Spread of each shot upways
@export_range(1, 16384) var shot_count: int = 1  # Amount of shots
@export_range(0, 25) var knockback: float = 1  # Amount of knockback

@export_subgroup("Sounds")
@export var sound_shoot: AudioStream  # Sound path

@export_subgroup("Crosshair")
@export var crosshair: Texture2D  # Image of crosshair on-screen
