extends Node3D

class_name NavigationController

@onready var nav_agent: NavigationAgent3D = $NavigationAgent3D
@onready var finger: RayCast3D = $Finger
@onready var parent: CharacterBody3D = get_node("..")
@onready var origin: Vector3 = self.global_position

var next_point ## Immediate next step to visit
var goal_position: Vector3 ## Ultimate desired location
var goal_target: Node3D ## Moving target to follow
#var step_position: Vector3 ## Move X meters past next point to make it less corner hugging
#var use_steps: bool = false

#var old_pos

var frame_tick_num: int

var zigzag: bool = false
var zig_then_zag: int = randi() % 2

var zagangle = 45
var zagstep = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	next_point = origin
	test_points()
	frame_tick_num = FrameTicker.get_frame_tick_queue()
	#print(frame_tick_num)
	nav_agent.max_speed = parent.SPEED
	nav_agent.avoidance_priority -= randf_range(0, 0.75)
	pass # Replace with function body.

func test_points():
	var shape = $Detect/CollisionShape3D
	var test = shape.shape.get_points()
	var resource = Resource.new()
	resource = test
	#resource.resource_path = "res://Data/Shapes/new_resource.tres"
	#print(resource)
	#print("shart")
	#ResourceSaver.save(resource, "res://Data/Shapes/new_resource.tres")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _physics_process(delta):
	#if parent.is_traveling == false:
		#return
	if parent.pathfinding_debug:
		pass
	#if nav_agent.is_navigation_finished() == true:
		#return
	next_point = nav_agent.get_next_path_position()
	#if randi_range(1, 60) > 1:
		#return
	if frame_tick_num != FrameTicker.frame_tick_current:
		return
	check_navigation()
	if zigzag:
		return
	if goal_target != null:
		move_to_point(goal_target.position)
	if parent.pathfinding_debug == true:
		#use_steps = true
		if nav_agent.is_target_reachable() == true:
			pass
		pass

func check_navigation():
	if get_target_distance(nav_agent.get_final_position()) <= nav_agent.target_desired_distance:
		end_travel()

func detect_player():
	if parent.player_in_viewcone:
		#parent.look_at(Vector3(parent.player.position.x, position.y, parent.player.position.z))
		finger.look_at(Vector3(parent.player.global_position.x, parent.player.global_position.y + 1.5, parent.player.global_position.z))
		if finger.is_colliding():
			var collider = finger.get_collider()
			if collider == parent.player:
				return true
			else:
				return false
		else:
			return false
	else:
		finger.rotation = Vector3(0,0,0)
		return false

func move_toward_object(object: Node3D):
	goal_target = object
	parent.look_at(Vector3(object.global_position.x, parent.position.y, object.global_position.z))
	move_to_point(object.position)

func move_to_point(x: Vector3):
	var fuzzy_movement = 0
	x.x += randf_range(-fuzzy_movement, fuzzy_movement)
	x.z += randf_range(-fuzzy_movement, fuzzy_movement)
	nav_agent.target_position = x
	parent.is_traveling = true

func zig_zag_towards_object(object: Node3D):
	zigzag = true
	#parent.look_at(Vector3(object.global_position.x, parent.position.y, object.global_position.z))
	var a = global_position
	#print(a, a + (parent.basis.z * 5))
	#print(parent.quaternion)
	var nubasis = -parent.basis.z
	var dx = nubasis.x
	var dz = nubasis.z
	var newdx = dx * cos(45) - dz * sin(45)
	var newdz = dx * sin(45) + dz * cos(45)
	var length = sqrt(newdx**2 + newdz**2)
	newdx /= length
	newdz /= length
	nubasis.x = newdx
	nubasis.z - newdz
	#print(parent.basis.z, "	", nubasis)
	#nubasis.z -= 0.45
	a = a + nubasis * 5
	move_to_point(a)

func zig_zag_towards_object2(object: Node3D):
	goal_target = object
	parent.look_at(Vector3(goal_target.global_position.x, parent.position.y, goal_target.global_position.z))
	zigzag = true
	#parent.global_rotate(Vector3.UP, zagangle)
	parent.rotate_y(deg_to_rad(zagangle))
	var a = global_position 
	a = a + -parent.basis.z * zagstep
	#parent.global_rotate(Vector3.UP, -zagangle*2)
	parent.rotate_y(deg_to_rad(-zagangle*2))
	var b = global_position
	b = b + -parent.basis.z * zagstep
	#parent.global_rotate(Vector3.UP, zagangle)
	parent.rotate_y(deg_to_rad(zagangle))
	var c = randi() % 2
	var d
	match c:
		0: ## Zig = 0
			d = a
			zig_then_zag = 1 ## Do zag next
		1: ## Zag = 1
			d = b
			zig_then_zag = 0 ## Do zig next
	move_to_point(d)
	
func zig_zag_update():
	#print("zig zag update")
	if get_target_distance(goal_target.position) < 2:
		zigzag = false
	var new_dest
	match zig_then_zag % 2:
		0:
			new_dest = get_zig()
		1:
			new_dest = get_zag()
	zig_then_zag += 1
	move_to_point(new_dest)

func get_zig():
	parent.look_at(Vector3(goal_target.global_position.x, parent.position.y, goal_target.global_position.z))
	#parent.global_rotate(Vector3.UP, zagangle)
	parent.rotate_y(deg_to_rad(zagangle))
	var a = global_position 
	a = a + -parent.basis.z * zagstep
	#parent.global_rotate(Vector3.UP, -zagangle)
	parent.rotate_y(deg_to_rad(-zagangle))
	return a

func get_zag():
	parent.look_at(Vector3(goal_target.global_position.x, parent.position.y, goal_target.global_position.z))
	#parent.global_rotate(Vector3.UP, -zagangle)
	parent.rotate_y(deg_to_rad(-zagangle))
	var b = global_position
	b = b + -parent.basis.z * zagstep
	#parent.global_rotate(Vector3.UP, zagangle)
	parent.rotate_y(deg_to_rad(zagangle))
	return b
	
func set_velocity_forced(velocity):
	nav_agent.set_velocity_forced(velocity)
	pass

func _on_detect_body_entered(body):
	if body == parent.player:
		parent.player_in_viewcone = true
		#nav_agent.target_desired_distance = 10

func _on_detect_body_exited(body):
	if body == parent.player:
		parent.player_in_viewcone = false
		#nav_agent.target_desired_distance = 1.5

func _on_navigation_agent_3d_target_reached():
	end_travel()

func get_target_distance(target_position: Vector3):
	var goal_target_dist = target_position - parent.position
	goal_target_dist = goal_target_dist.length()
	return goal_target_dist

func end_travel():
	if zigzag == true:
		if get_target_distance(goal_target.position) >= nav_agent.target_desired_distance:
			zig_zag_update()
			return
	if parent.is_traveling == true:
		parent.is_traveling = false
		goal_position = origin
		goal_target = null
		zigzag = false
		nav_agent.target_position = global_position
		parent.look_at(Vector3(parent.player.position.x, parent.position.y, parent.player.position.z))
		 
	return
