extends Resource
class_name Enemy

#enum NavigationMethods {FOLLOW}

@export var health: int
@export_subgroup("Attack")
@export var projectile_attack: bool
@export_range(1, 2048) var attack_range: int
@export_subgroup("Navigation")
#@export var default_chase: String = "move_toward_object"
@export_enum("move_toward_object", "move_to_point", "zig_zag_towards_object2") var default_chase: String
#@export var def: NavigationMethods
