extends CharacterBody3D

@export var pathfinding_debug = false
@export_range(0, 1, 0.1) var pathfinding_avoidance_priority: float = 1
@export var enemy_type: Enemy

#@onready var nav_agent: NavigationAgent3D = $NavigationAgent3D
@onready var NavController: NavigationController = $NavController
@onready var player: CharacterBody3D = get_tree().get_first_node_in_group("player")

const SPEED = 5.0
const JUMP_VELOCITY = 4.5
const FRICTION = 0.9

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

##Navigation
var nav_goal
var is_traveling: bool

##Enemy stuff
var player_in_viewcone: bool

##exp
@onready var default_chase = Callable(NavController, enemy_type.default_chase)

#test stuff
var use_steps: bool = false
var step_position: Vector3 
var step_length = 5
var need_new_step: bool = true

func _ready():
	#var callable = Callable(self, "shart")
	#callable.call()
	NavController.nav_agent.avoidance_priority = pathfinding_avoidance_priority

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
	else:
		velocity *= FRICTION
	#velocity += -basis.z * .1
	
	#else:
		#velocity.x = move_toward(velocity.x, 0, SPEED)
		#velocity.z = move_toward(velocity.z, 0, SPEED)
	
	#Navigation
	navigation()
	
	##Enemy stuff
	if player_in_viewcone:
		if NavController.detect_player():
			#queue_free()
			pass
		pass

	move_and_slide()

func navigation():
	if is_traveling == true:
		var travel_direction = NavController.next_point - global_position
		travel_direction = travel_direction.normalized()
		look_at(Vector3(NavController.next_point.x, position.y, NavController.next_point.z), Vector3.UP)
		#look_at(Vector3(player.position.x, position.y, player.position.z)) ##Look at player
		if use_steps == true:
			#print(step_position)
			if need_new_step == true:
				step_position = position + (travel_direction * step_length)
				need_new_step = false
			var step_remaining: Vector3 = step_position - position
			#print(step_remaining.length())
			look_at(Vector3(step_position.x, position.y, step_position.z))
			travel_direction = -basis.z
			#travel_direction = step_position - global_position
			#print(travel_direction)
			#travel_direction = travel_direction.normalized()
			velocity = travel_direction * SPEED
			velocity.y = 0
			if step_remaining.length() < 1:
				need_new_step = true
			return
			
		velocity = travel_direction * SPEED
		#velocity.y = 0
		if pathfinding_debug == true:
			#print(is_on_floor())
			#print(NavController.nav_agent.is_target_reachable())
			#use_steps = true
			pass
		NavController.set_velocity_forced(velocity)

func hit(damage, dealer):
	if pathfinding_debug:
		NavController.zig_zag_towards_object2(dealer)
		return
	#NavController.move_toward_object(dealer)
	#NavController.new_travel_goal(dealer.position)
	#NavController.zig_zag_towards_object2(dealer)
	if enemy_type.default_chase == "move_to_point":
		default_chase.call(dealer.position)
	if enemy_type.default_chase == "move_toward_object":
		default_chase.call(dealer)
	if enemy_type.default_chase == "zig_zag_towards_object2":
		default_chase.call(dealer)
